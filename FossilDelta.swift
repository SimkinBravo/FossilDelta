//
//  FossilDelta.swift
//  De la mano del diez
//
//  Created by Patricio Bravo Cisneros on 03/05/18.
//  Copyright © 2018 teleSUR. All rights reserved.
//

// Fossil SCM delta compression algorithm
// ======================================
//
// Format:
// http://www.fossil-scm.org/index.html/doc/tip/www/delta_format.wiki
//
// Algorithm:
// http://www.fossil-scm.org/index.html/doc/tip/www/delta_encoder_algorithm.wiki
//
// Original implementation:
// http://www.fossil-scm.org/index.html/artifact/d1b0598adcd650b3551f63b17dfc864e73775c3d
//
// LICENSE
// -------
//
// Copyright 2014 Dmitry Chestnykh (JavaScript port)
// Copyright 2007 D. Richard Hipp  (original C version)
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or
// without modification, are permitted provided that the
// following conditions are met:
//
//   1. Redistributions of source code must retain the above
//      copyright notice, this list of conditions and the
//      following disclaimer.
//
//   2. Redistributions in binary form must reproduce the above
//      copyright notice, this list of conditions and the
//      following disclaimer in the documentation and/or other
//      materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHORS ``AS IS'' AND ANY EXPRESS
// OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
// BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
// OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation
// are those of the authors and contributors and should not be interpreted
// as representing official policies, either expressed or implied, of anybody
// else.
//

import UIKit

infix operator >>> : BitwiseShiftPrecedence

func >>> (lhs: Int64, rhs: Int64) -> Int64 {
    return Int64(bitPattern: UInt64(bitPattern: lhs) >> UInt64(rhs))
}



class RollingHash: NSObject {
    // Hash window width in bytes. Must be a power of two.
    var zDigits: [UInt32] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz~".ascii

    var zValue: [Int32] = [  -1 , -1, -1, -1, -1, -1, -1, -1,   -1, -1, -1, -1, -1, -1, -1, -1,
                              -1, -1, -1, -1, -1, -1, -1, -1,   -1, -1, -1, -1, -1, -1, -1, -1,
                              -1, -1, -1, -1, -1, -1, -1, -1,   -1, -1, -1, -1, -1, -1, -1, -1,
                              0,  1,  2,  3,  4,  5,  6,  7,    8,  9, -1, -1, -1, -1, -1, -1,
                              -1, 10, 11, 12, 13, 14, 15, 16,   17, 18, 19, 20, 21, 22, 23, 24,
                              25, 26, 27, 28, 29, 30, 31, 32,   33, 34, 35, -1, -1, -1, -1, 36,
                              -1, 37, 38, 39, 40, 41, 42, 43,   44, 45, 46, 47, 48, 49, 50, 51,
                              52, 53, 54, 55, 56, 57, 58, 59,   60, 61, 62, -1, -1, -1, 63, -1]

    let NHASH: UInt16 = 16

    var a: UInt16 = 0       // hash     (16-bit unsigned)
    var b: UInt16 = 0       // values   (16-bit unsigned)
    var i = 0       // start of the hash window (16-bit unsigned)
    var z = [UInt8]() // the values that have been hashed.

    // Initialize the rolling hash using the first NHASH bytes of
    // z at the given position.
    convenience init(z: [UInt8], pos: Int) {
        self.init()
        var a: Int = 0
        var b: Int = 0
        for i in 0...NHASH {
            let x = z[pos.advanced(by: Int(i))]
            a = (a + Int(x)) & 0xffff
            b = (b + Int(NHASH - i) * Int(x)) & 0xffff
            self.z.append(x)
        }
        self.a = UInt16(a & 0xffff)
        self.b = UInt16(b & 0xffff)
        self.i = 0
    }

    // Advance the rolling hash by a single byte "c".
    func next(_ c: UInt8) {
        let old = UInt16(self.z[self.i])
        self.z[self.i] = c
        self.i = self.i.advanced(by: 1) & Int(NHASH.advanced(by: -1))
        self.a = (self.a - old + UInt16(c)) & 0xffff
        self.b = (self.b - NHASH * old + self.a) & 0xffff
    }

    // Return a 32-bit hash value.
    func value() -> UInt32 {
        return UInt32(Int64((self.a & 0xffff) | (self.b & 0xffff) << 16) >>> Int64(0))
    }

}

class FDReader {

    var a: [UInt8]!
    var pos = 0;

    var zDigits = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz~".ascii

    var zValue: [Int32] = [  -1 , -1, -1, -1, -1, -1, -1, -1,   -1, -1, -1, -1, -1, -1, -1, -1,
                            -1, -1, -1, -1, -1, -1, -1, -1,   -1, -1, -1, -1, -1, -1, -1, -1,
                            -1, -1, -1, -1, -1, -1, -1, -1,   -1, -1, -1, -1, -1, -1, -1, -1,
                            0,  1,  2,  3,  4,  5,  6,  7,    8,  9, -1, -1, -1, -1, -1, -1,
                            -1, 10, 11, 12, 13, 14, 15, 16,   17, 18, 19, 20, 21, 22, 23, 24,
                            25, 26, 27, 28, 29, 30, 31, 32,   33, 34, 35, -1, -1, -1, -1, 36,
                            -1, 37, 38, 39, 40, 41, 42, 43,   44, 45, 46, 47, 48, 49, 50, 51,
                            52, 53, 54, 55, 56, 57, 58, 59,   60, 61, 62, -1, -1, -1, 63, -1]

    // Reader reads bytes, chars, ints from array.
    convenience init(array: [UInt8]) {
        self.init()
        self.a = array      // source array
        self.pos = 0        // current position in array
    }

    func haveBytes() -> Bool {
        return self.pos < self.a.count
    }

    func getByte() -> UInt8 {
        let b = self.a[self.pos]
        self.pos += 1
        if self.pos > self.a.count {
            print("outOfBounds")
            return 0
        }
        return b
    }

    func getChar() -> String {
        let byte = getByte()
        return String(UnicodeScalar(UInt8(byte)))
    }

    func getInt() -> Int64 {
        var v: Int64 = 0
        var c: Int64 = 0
        while self.haveBytes() {
            c = Int64(zValue[Int(0x7f & self.getByte())])
            if c >= 0 {
                v = (v << 6) + c
            } else {
                break
            }
        }
        self.pos -= 1
        return  v >>> Int64(0)
    }
}

class Writer {

    var zDigits = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz~".ascii
    var a = [UInt8]()

    func toArray() -> [UInt8] {
        return a
    }

    func putByte(_ b: UInt8) {
        self.a.append(b & 0xff)
    }

    // Write an ASCII character (s is a one-char string).
    func putChar(_ s: Character) {
        self.putByte(UInt8(s.ascii!))
    }

    // Write a base64 unsigned integer.
    func putInt(_ v: Int64) {
        var zBuf = [UInt32]()
        if v == 0 {
            putChar("0")
        } else {
            var temp = 0
            var tempV = v
            for i in Int(tempV)...0 {
                if tempV > 0 {
                    zBuf.append(zDigits[Int(tempV & 0x3f)])
                    temp = i
                }
                tempV = tempV >>> Int64(6)
                temp += 1
            }
            for j in temp - 1...0 {
                putByte(UInt8(zBuf[j]))
            }
        }
    }

    // Copy from array at start to end.
    func putArray(a: [UInt8], start: Int, end: Int) {
        self.a.append(contentsOf: a[start...end - 1])
    }

}

class FossilDelta: NSObject {

    // Hash window width in bytes. Must be a power of two.
    let NHASH: Int32 = 16

    // Return the number digits in the base64 representation of a positive integer.
    func digitCount(_ v: UInt32) -> UInt32 {
        var i: UInt32 = 1
        var x = 64
        while v >= x {
            i += 1
            x <<= 6
        }
        return i
    }
    /*
     function digitCount(v){
     var i, x;
     for (i = 1, x = 64; v >= x; i++, x <<= 6){ /* nothing */ }
     return i;
     }
     */
    // Return a 32-bit checksum of the array.
    func checkSum(_ arr: [UInt8]) -> UInt8 {
        var sum0: UInt8 = 0, sum1: UInt8 = 0, sum2: UInt8 = 0, sum3: UInt8 = 0, z: Int = 0, N = arr.count
        //TODO measure if this unrolling is helpful.
        while N >= 16 {
            sum0 = sum0 + arr[z + 0] | 0
            sum1 = sum1 + arr[z + 1] | 0
            sum2 = sum2 + arr[z + 2] | 0
            sum3 = sum3 + arr[z + 3] | 0
            
            sum0 = sum0 + arr[z + 4] | 0
            sum1 = sum1 + arr[z + 5] | 0
            sum2 = sum2 + arr[z + 6] | 0
            sum3 = sum3 + arr[z + 7] | 0
            
            sum0 = sum0 + arr[z + 8] | 0
            sum1 = sum1 + arr[z + 9] | 0
            sum2 = sum2 + arr[z + 10] | 0
            sum3 = sum3 + arr[z + 11] | 0
            
            sum0 = sum0 + arr[z + 12] | 0
            sum1 = sum1 + arr[z + 13] | 0
            sum2 = sum2 + arr[z + 14] | 0
            sum3 = sum3 + arr[z + 15] | 0
            
            z += 16
            N -= 16
        }
        while N >= 4 {
            sum0 = sum0 + arr[z+0] | 0
            sum1 = sum1 + arr[z+1] | 0
            sum2 = sum2 + arr[z+2] | 0
            sum3 = sum3 + arr[z+3] | 0
            z += 4
            N -= 4
        }
        let s0 = (sum3 + (sum2 << 8) | 0)
        let s1 = (s0 + (sum1 << 16) | 0)
        sum3 = (s1 + (sum0 << 24) | 0)
        /* jshint -W086 */
        switch (N) {
            case 3: sum3 = sum3 + (arr[z+2] <<  8) | 0 /* falls through */
            case 2: sum3 = sum3 + (arr[z+1] << 16) | 0 /* falls through */
            case 1: sum3 = sum3 + (arr[z+0] << 24) | 0 /* falls through */
            default: print("default switch N")
        }
        return sum3 >> 0
    }

    // Create a new delta from src to out.
    func create(src: [UInt8], out: [UInt8]) -> [UInt8] {
        let zDelta = Writer()
        let lenOut = out.count
        let lenSrc = src.count
        var lastRead = -1
        var i: Int = 0
        zDelta.putInt(Int64(lenOut))
        zDelta.putChar("\n")

        // If the source is very small, it means that we have no
        // chance of ever doing a copy command.  Just output a single
        // literal segment for the entire target and exit.
        if lenSrc <= NHASH {
            zDelta.putInt(Int64(lenOut))
            zDelta.putChar(":")
            zDelta.putArray(a: out, start: 0, end: lenOut)
            zDelta.putInt(Int64(checkSum(out)))
            zDelta.putChar(";")
            return zDelta.toArray()
        }

        // Compute the hash table used to locate matching sections in the source.
        let nHash = Int(ceil(Double(lenSrc / Int(NHASH))))
        var collide =  Array(repeating: -1, count: nHash)
        var landmark = Array(repeating: -1, count: nHash)
        var hv: Int = 0
        for i in 0...lenSrc - Int(NHASH) {
            let h = RollingHash(z: src, pos: i)
            hv = Int(h.value() % UInt32(nHash))
            collide[i / Int(NHASH)] = landmark[hv]
            landmark[hv] = i / Int(NHASH)
        }

        var base: Int32 = 0
        var iSrc: UInt32, iBlock: UInt32, bestCnt: UInt32, bestOfst: UInt32, bestLitsz: UInt32

        while (base.advanced(by: Int(NHASH)) < lenOut) {
            bestOfst = 0
            bestLitsz = 0
            let h = RollingHash(z: out, pos: Int(base))
            i = 0 // Trying to match a landmark against zOut[base+i]
            bestCnt = 0
            while(true) {
                var limit = 250
                hv = Int(h.value() % UInt32(nHash))
                iBlock = UInt32(landmark[hv])
                while iBlock >= 0 && limit > 0 {
                    limit -= 1
                    //
                    // The hash window has identified a potential match against
                    // landmark block iBlock.  But we need to investigate further.
                    //
                    // Look for a region in zOut that matches zSrc. Anchor the search
                    // at zSrc[iSrc] and zOut[base+i].  Do not include anything prior to
                    // zOut[base] or after zOut[outLen] nor anything after zSrc[srcLen].
                    //
                    // Set cnt equal to the length of the match and set ofst so that
                    // zSrc[ofst] is the first element of the match.  litsz is the number
                    // of characters between zOut[base] and the beginning of the match.
                    // sz will be the overhead (in bytes) needed to encode the copy
                    // command.  Only generate copy command if the overhead of the
                    // copy command is less than the amount of literal text to be copied.
                    //
                    var cnt: UInt32, ofst: UInt32, litsz: UInt32
                    var j = 0, k = 1, x: UInt32, y = base + 1
                    
                    // Beginning at iSrc, match forwards as far as we can.
                    // j counts the number of characters that match.
                    iSrc = iBlock.unsafeMultiplied(by: UInt32(NHASH))
                    x = iSrc
                    while (x < lenSrc && y < lenOut) {
                        j += 1
                        x += 1
                        y += 1
                        if (src[Int(x)] != out[Int(y)]) {
                            break
                        }
                    }
                    j -= 1
                    
                    // Beginning at iSrc-1, match backwards as far as we can.
                    // k counts the number of characters that match.
                    while k < iSrc && k <= i {
                        k += 1
                        if (src[Int(iSrc.advanced(by: k * -1))] != out[Int(base.advanced(by: i).advanced(by: k * -1))]) {
                            break
                        }
                    }
                    k -= 1
                    
                    // Compute the offset and size of the matching region.
                    ofst = iSrc.advanced(by: k * -1)
                    cnt = UInt32(j.advanced(by: k).advanced(by: 1))
                    litsz = UInt32(i-k)  // Number of bytes of literal text before the copy
                    // sz will hold the number of bytes needed to encode the "insert"
                    // command and the copy command, not counting the "insert" text.
                    let sz = digitCount(UInt32(i-k)) + digitCount(cnt) + digitCount(ofst) + 3
                    if (cnt >= sz && cnt > bestCnt) {
                        // Remember this match only if it is the best so far and it
                        // does not increase the file size.
                        bestCnt = cnt
                        bestOfst = iSrc.advanced(by: k * -1)
                        bestLitsz = litsz
                    }
                    
                    // Check the next matching block
                    iBlock = UInt32(collide[Int(iBlock)])
                }
                
                // We have a copy command that does not cause the delta to be larger
                // than a literal insert.  So add the copy command to the delta.
                if (bestCnt > 0) {
                    if (bestLitsz > 0) {
                        // Add an insert command before the copy.
                        zDelta.putInt(Int64(bestLitsz))
                        zDelta.putChar(":")
                        zDelta.putArray(a: out, start: Int(base), end: Int(base.advanced(by: Int(bestLitsz))))
                        base += Int32(bestLitsz)
                    }
                    base += Int32(bestCnt)
                    zDelta.putInt(Int64(bestCnt))
                    zDelta.putChar("@")
                    zDelta.putInt(Int64(bestOfst))
                    zDelta.putChar(",")
                    if (bestOfst + bestCnt - 1 > lastRead) {
                        lastRead = Int(bestOfst + bestCnt.advanced(by: -1))
                    }
                    bestCnt = 0
                    break
                }

                // If we reach this point, it means no match is found so far
                if (base.advanced(by: i).advanced(by: Int(NHASH)) >= lenOut) {
                    // We have reached the end and have not found any
                    // matches.  Do an "insert" for everything that does not match
                    zDelta.putInt(Int64(lenOut.advanced(by: Int(base * -1))))
                    zDelta.putChar(":")
                    zDelta.putArray(a: out, start: Int(base), end: Int(base.advanced(by: lenOut).advanced(by: Int(base * -1))))
                    base = Int32(lenOut)
                    break
                }
                
                // Advance the hash by one character. Keep looking for a match.
                h.next(out[Int(base.advanced(by: i).advanced(by: Int(NHASH)))])
                i += 1
            }
        }
        // Output a final "insert" record to get all the text at the end of
        // the file that does not match anything in the source.
        if(base < lenOut) {
            zDelta.putInt(Int64(lenOut.advanced(by: Int(base * -1))))
            zDelta.putChar(":")
            zDelta.putArray(a: out, start: Int(base), end: Int(base.advanced(by: lenOut).advanced(by: Int(base * -1))))
        }
        // Output the final checksum record.
        zDelta.putInt(Int64(Int(checkSum(out))))
        zDelta.putChar(";")
        return zDelta.toArray()

    }

    func outputSize(delta: [UInt8]) -> Int {
        let zDelta = FDReader(array: delta)
        let size = zDelta.getInt()
        if (zDelta.getChar() != "\n") {
            print("size integer not terminated by \'\\n\'")
            return 0
        }
        return Int(size)
    }

    // Apply a delta.
    func apply(src: [UInt8], delta: [UInt8], opts: [String: AnyObject]! = nil) -> [UInt8]! {
        var total: Int64 = 0
        let zDelta = FDReader(array: delta)
        let lenSrc = src.count
        let lenDelta = delta.count
        
        let limit = zDelta.getInt()
        if (zDelta.getChar() != "\n") {
            print("size integer not terminated by \'\\n\'")
            return nil
        }
        let zOut = Writer()
        while(zDelta.haveBytes()) {
            var ofst: Int = 0
            let cnt = zDelta.getInt()
//            print("cnt#", cnt, lenSrc)
            switch (zDelta.getChar()) {
            case "@":
                ofst = Int(zDelta.getInt())
                if (zDelta.haveBytes() && zDelta.getChar() != ",") {
                    print("copy command not terminated by \',\'")
                    return nil
                }
                total += cnt
                if (total > limit) {
                    print("copy exceeds output file size")
                    return nil
                }
                if (ofst.advanced(by: Int(cnt)) > lenSrc) {
                    print("copy extends past end of input")
                    return nil
                }
                zOut.putArray(a: src, start: ofst, end: ofst.advanced(by: Int(cnt)))

            case ":":
                total += cnt
                if total > limit {
                    print("insert command gives an output larger than predicted")
                    return nil
                }
                if cnt > lenDelta {
                    print("insert count exceeds size of delta")
                    return nil
                }
                zOut.putArray(a: zDelta.a, start: zDelta.pos, end: zDelta.pos.advanced(by: Int(cnt)))
                zDelta.pos += Int(cnt)

            case ";":
                let out = zOut.toArray()
/*
                if ((opts == nil || opts["verifyChecksum"] as! Bool != false) && cnt != checkSum(out)) {
                    print("bad checksum")
                    return nil
                }
*/
                if (total != limit) {
                    print("generated size does not match predicted size")
                    return nil
                }
                return out

            default:
                print("unknown delta operator")
                return nil
            }
        }
        print("unterminated delta")
        return nil
    }

}
